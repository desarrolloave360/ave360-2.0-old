import { Component, Input, Output, EventEmitter, ElementRef } from '@angular/core';

@Component({
	selector: 'app-radio-button',
	styleUrls: ['./radio-button.component.css'],
	template: `
			<button
				type='button'
				class='btn btn-circle'
				*ngIf='!icon'
				(click)='onClick()'
				[disabled]="isDisabled || disabled"
				[ngClass]="active ? activeClass : defaultClass">
				<i class="fa fa-lg" ng-class="iconClass" ng-if="active"></i>
			</button>
			<i class="fa fa-lg" ng-class="icon" ng-click="toogleCheck()" ng-style="typeIconStyle()" ng-if="icon"></i>
	`
})
export class RadioButtonComponent {

	@Input() active: boolean;
	@Input('disabled') isDisabled: boolean;
	@Input() iconClass = 'fa-check';
	@Output() activeChange: EventEmitter<boolean> = new EventEmitter();

	defaultClass = 'btn-default';
	activeClass = 'btn-primary';
	disabled: boolean;

	constructor(private element: ElementRef) {
		if (this.element.nativeElement.attributes['disabled'] != null
		&& this.element.nativeElement.attributes['disabled'].value == '') {
			this.disabled = true;
		}
	}

	onClick() {
		// if (this.active == null) {
		// 	console.error('Property active must be present');
		// 	return;
		// }
		setTimeout(() => {
			this.clearAll();
			this.active = true;
			this.activeChange.emit(this.active);
		}, 100);
	}

	clearAll() {
		const radios = [];
		const query = document.getElementsByTagName('app-radio-button');
		console.log(query);

		for (let i = 0; i < query.length; i++) {
			const radioBtn = query[i];

			if (this.element.nativeElement.attributes[name] != null) {
				if (this.element.nativeElement.attributes[name].value == radioBtn.attributes[name].value) {
					radios.push(radioBtn);
				}
			} else {
				if (radioBtn.attributes[name] == null) {
					radios.push(radioBtn);
				}
			}
		}
	}

}
