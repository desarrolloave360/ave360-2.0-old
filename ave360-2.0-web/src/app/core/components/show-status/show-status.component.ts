import { Component, Input } from '@angular/core';

@Component({
	selector: 'app-show-status',
	template: `{{status ? 'ACTIVO' : 'INACTIVO'}}`
})
export class ShowStatusComponent {
	@Input('status') status: boolean;

	constructor() {}
}
