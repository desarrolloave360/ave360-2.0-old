import { Component, Input, ElementRef } from '@angular/core';

@Component({
	selector: 'app-status-selector',
	template: `
                <ng-select [items]= "['ACTIVO','INACTIVO']"
                       bindLabel="status ? 'ACTIVO' : 'INACTIVO'"
                       [(ngModel)]="status"
                       placeholder="Seleccione un estado..."
                       [disabled]="isDisabled || disabled">
                </ng-select>
    `
})
export class StatusSelectorComponent {
	@Input('isDisabled') isDisabled = false;

	status: string;
	disabled: boolean;

	constructor(private element: ElementRef) {
		if (this.element.nativeElement.attributes['disabled'] != null
		&& this.element.nativeElement.attributes['disabled'].value == '') {
			this.disabled = true;
		}
	}

	public getStatus() {
		if (this.status === 'ACTIVO') {
			return true;
		} else if (this.status === 'INACTIVO') {
			return false;
		} else {
			return null;
		}
	}

	public setStatusToNull() {
		this.status = null;
	}
}
