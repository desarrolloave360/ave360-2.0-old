import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';

import * as baseConstants from '../../base-constants';

@Component({
	selector: 'app-pagination',
	template: '<ngb-pagination [collectionSize]="totalPages" [(page)]="page" (pageChange)="pageChanged()"></ngb-pagination>',
	providers: [NgbPaginationConfig]
})
export class PaginationComponent {
	@Input('collectionSize') totalPages: number;
	@Input('page') page: number;
	@Output('pageState') pageState = new EventEmitter<number>();

	constructor(config: NgbPaginationConfig) {
		config.maxSize = 3;
		config.pageSize = baseConstants.tableSize;
		config.ellipses = false;
		config.boundaryLinks = true;
		config.size = 'sm';
	}

	pageChanged() {
		this.pageState.emit(this.page);
	}

}
