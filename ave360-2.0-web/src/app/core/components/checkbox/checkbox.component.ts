import { Component, Input, Output, EventEmitter, ElementRef, AfterViewInit } from '@angular/core';

@Component({
	selector: 'app-checkbox',
	styleUrls: ['./checkbox.component.css'],
	template: `
		<button
			type='button'
			class='btn btn-xs'
			(click)='onClick()'
			[ngClass]='active ? activeClass : defaultClass'
			[disabled]='isDisabled || disabled'>
				<i class='fa fa-check' *ngIf='active'></i>
		</button>
    `
})
export class CheckboxComponent implements AfterViewInit {

	@Input('disabled') isDisabled: boolean;
	@Input() active: boolean;
	@Output() activeChange: EventEmitter<boolean> = new EventEmitter();

	defaultClass = 'btn-default';
	activeClass = 'btn-primary';
	disabled: boolean;

	constructor(private element: ElementRef) {
		if (this.element.nativeElement.attributes['disabled'] != null
		&& this.element.nativeElement.attributes['disabled'].value == '') {
			this.disabled = true;
		}
	}

	ngAfterViewInit(): void {
		const childElement = this.element.nativeElement.firstElementChild;
		if (this.element.nativeElement.attributes['medium'] != null) {
			this.removeBtnClass(childElement);
			jQuery(childElement).addClass('btn-sm');
			childElement.style['width'] = '34px';
			childElement.style['height'] = '34px';
		}

		if (this.element.nativeElement.attributes['large'] != null) {
			this.removeBtnClass(childElement);
			childElement.style['width'] = '40px';
			childElement.style['height'] = '40px';
		}

		if (this.element.nativeElement.attributes['big'] != null) {
			this.removeBtnClass(childElement);
			jQuery(childElement).addClass('btn-lg');
			childElement.style['width'] = '52px';
			childElement.style['height'] = '52px';
		}

		if (this.element.nativeElement.attributes['success'] != null) this.activeClass = 'btn-success';
		if (this.element.nativeElement.attributes['warning'] != null) this.activeClass = 'btn-warning';
		if (this.element.nativeElement.attributes['danger'] != null) this.activeClass = 'btn-danger';
		if (this.element.nativeElement.attributes['info'] != null) this.activeClass = 'btn-info';
	}

	onClick() {
		if (this.active == null) {
			console.error('Property active must be present');
			return;
		}
		setTimeout(() => {
			this.active = !this.active;
			this.activeChange.emit(this.active);
		}, 100);
	}

	removeBtnClass(childelement) {
		jQuery(childelement).removeClass('btn-xs');
		jQuery(childelement).removeClass('btn-sm');
		jQuery(childelement).removeClass('btn-lg');
	}
}
