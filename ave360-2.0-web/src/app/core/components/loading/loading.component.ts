import { Component } from '@angular/core';

@Component({
	selector: 'app-loading',
	template: `
            <div class="table-type">
	            <div class="sk-spinner sk-spinner-three-bounce table-cell-centered">
	                <div class="sk-bounce1"></div>
	                <div class="sk-bounce2"></div>
	                <div class="sk-bounce3"></div>
	            </div>
			</div>
    `
})
export class LoadingComponent {

	constructor() {}
}
