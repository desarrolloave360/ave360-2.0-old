import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

import * as baseConstants from '../core/base-constants';

@Injectable()
export class RestInterceptor implements HttpInterceptor {
	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
	if (localStorage.getItem(baseConstants.cookieToken)) {
		const tokenType = 'X-' + baseConstants.cookieToken;
		const copiedReq = req.clone({
			headers: req.headers
			.set(tokenType, localStorage.getItem(baseConstants.cookieToken))
			.set('Accept', 'application/json, text/plain, */*')
			.set('Content-Type', 'application/json;charset=utf-8')
		});
			return next.handle(copiedReq);
		}
		return next.handle(req);
	}
}
