import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BsDropdownModule } from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { DataTablesModule } from 'angular-datatables';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// COMPONENTS
import { StatusSelectorComponent } from './components/status-selector/status-selector.component';
import { ShowStatusComponent } from './components/show-status/show-status.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { LoadingComponent } from './components/loading/loading.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { HelpblockComponent } from './components/help-block/help-block.component';
import { RadioButtonComponent } from './components/radio-button/radio-button.component';

// DIRECTIVES
import { DropdownDirective } from './directives/dropdown.directive';
import { ValidationDirective } from './directives/validation.directive';

// SERVICES
import { RestService } from './services/rest.service';
import { NotifService } from './services/notif.service';
import { AuthenticateService } from './services/authenticate.service';
import { GetDTOptionsService } from './services/get-dtoptions.service';
import { SweetService } from './services/sweet.service';

@NgModule({
	declarations: [
		HelpblockComponent,
		StatusSelectorComponent,
		ShowStatusComponent,
		PaginationComponent,
		LoadingComponent,
		CheckboxComponent,
		RadioButtonComponent,
		DropdownDirective,
		ValidationDirective
	],
	imports: [
		FormsModule,
		BsDropdownModule.forRoot(),
		ToastrModule.forRoot(),
		NgbModule.forRoot(),
		BrowserAnimationsModule,
		DataTablesModule,
		NgSelectModule
	],
	exports: [
		CommonModule,
		FormsModule,
		DataTablesModule,
		NgSelectModule,
		NgbModule,
		StatusSelectorComponent,
		ShowStatusComponent,
		PaginationComponent,
		LoadingComponent,
		CheckboxComponent,
		RadioButtonComponent,
		DropdownDirective,
		ValidationDirective
	],
	providers: [
		RestService,
		NotifService,
		AuthenticateService,
		GetDTOptionsService,
		SweetService
	],
	entryComponents: [
		HelpblockComponent
	]
})
export class CoreModule {}
