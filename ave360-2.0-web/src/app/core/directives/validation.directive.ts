import {
	Directive,
	DoCheck,
	AfterViewChecked,
	ElementRef,
	ComponentFactory,
	ComponentRef,
	ViewContainerRef,
	ComponentFactoryResolver
} from '@angular/core';
import 'jquery-slimscroll';
import {NgModel} from '@angular/forms';

import { HelpblockComponent } from '../components/help-block/help-block.component';

import * as baseConstants from '../../core/base-constants';

declare var jQuery: any;
declare var $: any;

@Directive({
	selector: '[appValidation]'
})
export class ValidationDirective implements DoCheck, AfterViewChecked {

	toValidate: ElementRef;
	form: ElementRef;
	componentRef: ComponentFactory<HelpblockComponent>;
	helpblock: ComponentRef<HelpblockComponent>;
	inInputGroup = false;

	constructor(private element: ElementRef,
				private viewContainer: ViewContainerRef,
				private componentFactoryResolver: ComponentFactoryResolver,
				private ngModel: NgModel) {

		this.toValidate = this.element.nativeElement.closest('.to-validate');
		this.form = element.nativeElement.form;
		this.componentRef = this.componentFactoryResolver.resolveComponentFactory(HelpblockComponent);

	}

	ngDoCheck() {
		if (this.ngModel.formDirective.submitted) {
			jQuery(this.element.nativeElement).addClass('ng-dirty');
			jQuery(this.element.nativeElement).addClass('ng-touched');
		}

		if (jQuery(this.element.nativeElement).hasClass('ng-invalid')
				&& jQuery(this.element.nativeElement).hasClass('ng-dirty')
				&& jQuery(this.element.nativeElement).hasClass('ng-touched')) {

			if (!jQuery(this.toValidate).hasClass('has-error')) {
				if (jQuery(this.element.nativeElement.parentElement).hasClass('input-group')) {
					this.inInputGroup = true;
					$('<small class="text-danger"#helpBlock>' + this.getErrorText() +
						'</small>').insertAfter(this.element.nativeElement.closest('.input-group'));
				} else {
					setTimeout(() => {
						this.helpblock = this.viewContainer.createComponent(this.componentRef);
						this.helpblock.instance.message = this.getErrorText();
					}, 100);
				}

				jQuery(this.toValidate).addClass('has-error');
			}
		} else {
			if (jQuery(this.toValidate).hasClass('has-error')) {
				jQuery(this.toValidate).removeClass('has-error');
				if (this.inInputGroup) {
					jQuery(this.element.nativeElement.closest('.to-validate').children).find('.text-danger').remove();
				}
			}

			if (this.helpblock) {
				this.helpblock.destroy();
			}
		}
	}

	ngAfterViewChecked() {
		if (this.element.nativeElement.attributes['required']) {
			jQuery(this.toValidate).addClass('label-required');
		} else {
			jQuery(this.toValidate).removeClass('label-required');
		}
	}

	getErrorText() {
		let text = '';

		Object.keys(this.ngModel.errors).forEach(key => {
			text += baseConstants.errorMessages[key] + ', ';
		});

		return text.substring(0, text.length - 2);
	}

}
