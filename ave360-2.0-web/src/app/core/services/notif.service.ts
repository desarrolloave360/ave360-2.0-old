import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import * as baseConstants from '../../core/base-constants';

@Injectable()
export class NotifService {

	options = {
		timeOut: baseConstants.notifyTime,
		closeButton: true,
		messageClass: 'toast-title',
		tapToDismiss: false
	};

	constructor(private toastr: ToastrService) {}


	success(message: string) {
		this.toastr.success(message, '', this.options);
	}

	danger(message: string) {
		this.toastr.error(message, '', this.options);
	}

	warning(message: string) {
		this.toastr.warning(message, '', this.options);
	}

	info(message: string) {
		this.toastr.info(message, '', this.options);
	}

	show() {}
}
