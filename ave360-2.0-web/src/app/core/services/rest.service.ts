import { HttpClient, HttpRequest } from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Injectable } from '@angular/core';

import { NotifService } from './notif.service';
import * as baseConstants from '../../core/base-constants';
import { environment } from 'environments/environment';

@Injectable()
export class RestService {

	private baseUrl: string = environment.baseUrl;

	constructor(private httpClient: HttpClient, private router: Router, private notif: NotifService) {}

	post(url: String, data?) {
		const finalUrl = this.baseUrl + url;
		const req = new HttpRequest('POST', finalUrl, data, {headers: null, responseType: 'json', withCredentials: true});
		return this.httpClient.request(req).pipe(catchError(this.handleError));
	}

	get(url: String, params?) {
		let finalUrl = this.baseUrl + url;
		if (params != null) {
		Object.keys(params).forEach(key => {
			finalUrl += ('/' + params[key]);
		});
		}
		const req = new HttpRequest('GET', finalUrl, {observe: 'body', responseType: 'json', withCredentials: true});
		return this.httpClient.request(req).pipe(catchError(this.handleError));
	}

	private handleError(response: any) {
		if (response.error instanceof ErrorEvent) {
			console.error('Ha ocurrido un error: ', response.error.message);
		} else {
			if (response.status == 403) {
			this.notif.danger(baseConstants.messages.expired);
			this.router.navigate(['/login']);
			}

			if (response.status == 401) {
			this.notif.danger(baseConstants.messages.unauthorized);
			this.router.navigate(['/login']);
			}

			if (response.status == 500) {
				if (response.message == 'Unauthorized') {
					this.router.navigate(['/login']);
				} else {
					this.notif.danger(response.data.message);
				}
			}

			if (response.status == 501) {
				this.notif.danger(response.data.message);
			}

			if (response.status <= 0) {
				response.data = {message: baseConstants.messages.unableToConnect, status: 500};
			}

			if (response.status == 406) {
				if (response.data.message == 'No message available') {
					this.notif.danger('Error ' + response.data.status + ': ' + response.data.error);
				} else {
					this.notif.danger(response.data.message);
				}
			}
		}
		return throwError(response);
	}
}
