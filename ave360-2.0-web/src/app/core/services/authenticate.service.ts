import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as baseConstants from '../../core/base-constants';
import { environment } from 'environments/environment';

@Injectable()
export class AuthenticateService {

	public constructor(private httpClient: HttpClient) {}

	authenticate(credentials: {username: string, password: string}) {
		const auth = baseConstants.authHeaderPrefix + btoa(credentials.username + baseConstants.authHeaderSeparator + credentials.password +
															baseConstants.authHeaderSeparator + baseConstants.authRepo);
		const headers = new HttpHeaders().set(baseConstants.authHeader, auth);
		const req = new HttpRequest('GET', environment.baseUrl + 'authentication/user',
									{responseType: 'text', headers: headers, withCredentials: true});
		return this.httpClient.request(req);
	}
}
