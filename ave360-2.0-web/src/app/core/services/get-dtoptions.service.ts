import { Injectable } from '@angular/core';

import * as baseConstants from '../../core/base-constants';

const data = {
	emptyTable:          'No hay resultados con los criterios indicados.',
	info:                'Mostrando _START_ al _END_  de _TOTAL_ registro(s).',
	infoEmpty:           '',
	infoFiltered:        '(filtrado de _MAX_ resgistro(s) en total)',
	infoThousands:       ',',
	lengthMenu:          'Mostrar _MENU_ registros',
	loadingRecords:      'Cargando...',
	processing:          'Procesando...',
	search:              'Filtro:',
	zeroRecords:         '',
	paginate: {
		first:           'Primero',
		last:            'Último',
		next:            'Siguiente',
		previous:        'Anterior'
	}
}

@Injectable()
export class GetDTOptionsService {
	scope: {content?, totalElements?, numberOfElements?, totalPages?, begining?, end?} = {};

	unpaged(htmlOptions?) {
		if (htmlOptions == null) {
		htmlOptions = 'Tgitp';
	}

	const dtOptions: DataTables.Settings = {
		pageLength: baseConstants.tableSize,
		dom: '<"html5buttons"B>' + htmlOptions,
		order: [],
		language: data,
		columnDefs: []
	}

	return dtOptions
	}

	paged() {
		const dtOptions: DataTables.Settings = {
			pageLength: baseConstants.tableSize,
			dom: '<"html5buttons"B>Tgit',
			order: [],
			language: data,
			columnDefs: [],
			infoCallback: (settings, start, end, max, total, pre) => {
				if (this.scope.totalElements == null) {
					return '';
				} else {
					if (this.scope.totalElements == 0) {
						return '';
					} else {
						return 'Mostrando ' + this.scope.begining + ' al ' + this.scope.end + ' de ' + this.scope.totalElements + ' registro(s).';
					}
				}
			}
		}

		return dtOptions;
	}

	// tslint:disable-next-line:no-shadowed-variable
	setPagedData(data: {content, totalElements, number, numberOfElements, totalPages}) {
		this.scope.totalElements = data.totalElements;
		this.scope.begining = (baseConstants.tableSize * data.number) + 1;
		this.scope.end = this.scope.begining + data.numberOfElements - 1;
		return data.content;
	}

	notSortable(columns: number | [number]) {
		const dtDefs: DataTables.ColumnDefsSettings = {targets: columns};
		dtDefs.orderable = false;
		return dtDefs;
	}

	notSortableAll(quantity: number) {
		const columns = [];
		for (let i = 0 ; i < quantity ; i++) {
			columns.push(i);
		}
		const dtDefs: DataTables.ColumnDefsSettings = {targets: columns};
		dtDefs.orderable = false;
		return dtDefs;
	}

	rerender(scope): void {
		scope.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
			// Destroy the table first
			dtInstance.destroy();
			// Call the dtTrigger to rerender again
			scope.dtTrigger.next();
		});
	}
}
