import { Injectable } from '@angular/core';
import Swal from 'sweetalert2'

import * as baseConstants from '../../core/base-constants';

@Injectable()
export class SweetService {

	constructor() {}

	default(message, confirm, cancel?) {
		Swal({
		title: baseConstants.messages.sure,
		text: message,
		type: 'info',
		confirmButtonColor: '#1ab394',
		showCancelButton: true,
		confirmButtonText: '¡Sí, aceptar!',
		cancelButtonText: '¡No, cancelar!',
		width: '478px',
		focusConfirm: false,
		reverseButtons: true,
		showLoaderOnConfirm: true
		}).then((result) => {
			if (result.value) {
				confirm();
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				if (cancel == null) {
					Swal.close();
				} else {
					cancel();
				}
			}
		});
	}

	defaultOk(message, confirm) {
		Swal({
			title: '',
			text: message,
			type: 'info',
			confirmButtonColor: '#1ab394',
			showCancelButton: false,
			confirmButtonText: 'Aceptar',
			width: '478px',
			reverseButtons: true,
			showLoaderOnConfirm: true
		}).then((result) => {
			if (result.value) {
				confirm();
			}
		});
	}

	save(request?: Function, scope?) {
		return Swal({
			title: baseConstants.messages.sure,
			text: baseConstants.messages.confirmation,
			type: 'info',
			confirmButtonColor: '#1ab394',
			showCancelButton: true,
			confirmButtonText: '¡Sí, aceptar!',
			cancelButtonText: '¡No, cancelar!',
			width: '478px',
			reverseButtons: true,
			showLoaderOnConfirm: true,
			preConfirm: () => {
				if (request != null) return new Promise(() => {request(scope)});
			}
		});
	}

	changeStatus(request?: Function, scope?) {
		return Swal({
			title: baseConstants.messages.sure,
			text: 'Se cambiará el estado del registro seleccionado.',
			type: 'warning',
			confirmButtonColor: '#1ab394',
			showCancelButton: true,
			confirmButtonText: '¡Sí, aceptar!',
			cancelButtonText: '¡No, cancelar!',
			width: '478px',
			reverseButtons: true,
			showLoaderOnConfirm: true,
			preConfirm: () => {
				if (request != null) return new Promise(() => {request(scope)});
			}
		});
	}

	error(message) {
		Swal('Error', message, 'error');
	}

	close() {
		Swal.close();
	}

}
