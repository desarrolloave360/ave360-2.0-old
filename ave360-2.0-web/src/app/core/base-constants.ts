// ------------------------------------------------------
// ---------------------- SECURITY ----------------------
// ------------------------------------------------------

export const	authHeader = 'authorization';

export const	authHeaderPrefix = 'Basic ';

export const	authHeaderSeparator = ':';

export const	cookieToken = 'XSRF-TOKEN';

export const    authRepo = 'USUINT';

export const	loginState = 'login';

export const	mainState = 'main.dashboard';

export const	emailMask = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+(\.[A-Za-z]{2,4})+$/;

export const	isSadegData = true;


// ------------------------------------------------------
// ------------------------- END ------------------------
// ------------------------------------------------------

export const delayTime = 100;

export const tableSize = 20;

export const notifyTime = 1000;

export const maxSizeForMail = 10;

export const errorMessages = {
	required: 'Dato requerido.'
};

export const messages = {
	sure: '¿Estás seguro?',
	confirmation: 'Se guardará la información.',
	expired: 'La sesión ha expirado.',
	unauthorized: 'No autorizado.',
	unableToConnect: 'No se pudo conectar al servidor.',
};

export const acronyms = {

};
