import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import { OrderModule } from 'ngx-order-pipe';
import { NgbActiveModal, NgbDateAdapter, NgbDateNativeAdapter } from '@ng-bootstrap/ng-bootstrap';

// Modules
import { CommonsModule } from './commons/commons.module';
import { CoreModule } from './core/core.module';
import { CarClaimsModule } from './modules/car-claims/car-claims.module';

import { AppComponent } from './app.component';
import { appRoutes } from './app.routes';
import { RestInterceptor } from './core/rest.interceptor';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		RouterModule.forRoot(appRoutes, { useHash: true }),
		CommonsModule,
		OrderModule,
		CoreModule,
		CarClaimsModule
	],
	providers: [
		{provide: APP_BASE_HREF,  useValue: '/'},
		{provide: HTTP_INTERCEPTORS, useClass: RestInterceptor, multi: true},
		{provide: NgbDateAdapter, useClass: NgbDateNativeAdapter},
		NgbActiveModal
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
