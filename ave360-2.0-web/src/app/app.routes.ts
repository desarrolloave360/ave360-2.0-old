import {Routes} from '@angular/router';

import { BasicLayoutComponent } from './commons/layouts/basicLayout.component';
import { StarterViewComponent } from './commons/dashboard/dashboard.component';
import { BlankLayoutComponent } from './commons/layouts/blankLayout.component';
import { LoginComponent } from './commons/login/login.component';


export const appRoutes: Routes = [
	// Main redirect
	{path: '', redirectTo: 'starterview', pathMatch: 'full'},

	{
		path: '', component: BasicLayoutComponent,
		children: [
			{path: 'starterview', component: StarterViewComponent, data: {displayName: 'Inicio', abstract: false}}
		], data: {displayName: 'Inicio', abstract: false}
	},

	{
		path: '', component: BlankLayoutComponent,
		children: [
			{ path: 'login', component: LoginComponent },
		]
	},

	{path: 'param', loadChildren: './modules/car-claims/commission-agent/commision-agent.module#CommissionAgentModule'},

	// Handle all other routes
	{path: '**',  redirectTo: 'starterview'}
];
