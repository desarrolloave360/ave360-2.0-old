import { Component, OnDestroy, OnInit, } from '@angular/core';


@Component({
	templateUrl: './dashboard.component.html'
})
export class StarterViewComponent implements OnDestroy, OnInit  {
	today = new Date();
	public nav: any;

	public constructor() {
		this.nav = document.querySelector('nav.navbar');
	}

	public ngOnInit(): any {
		this.nav.className += ' white-bg';
	}

	public ngOnDestroy(): any {
		this.nav.classList.remove('white-bg');
	}

}
