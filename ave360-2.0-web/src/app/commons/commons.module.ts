import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import { OrderModule } from 'ngx-order-pipe';

import { StarterViewComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { FooterComponent } from './footer/footer.component';
import { BasicLayoutComponent } from './layouts/basicLayout.component';
import { BlankLayoutComponent } from './layouts/blankLayout.component';
import { BlankComponent } from './layouts/blank.component';
import { NavigationComponent } from './navigation/navigation.component';
import { TopNavbarComponent } from './topnavbar/topnavbar.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { NavigationRendererComponent } from './navigation/navigation-renderer/navigation-renderer.component';
import { CoreModule } from '../core/core.module';


@NgModule({
	declarations: [
		StarterViewComponent,
		LoginComponent,
		FooterComponent,
		BasicLayoutComponent,
		BlankLayoutComponent,
		BlankComponent,
		NavigationComponent,
		TopNavbarComponent,
		BreadcrumbComponent,
		NavigationRendererComponent
	],
	imports: [
		BrowserModule,
		RouterModule,
		OrderModule,
		CoreModule
	],
	exports: [
		StarterViewComponent,
		LoginComponent,
		FooterComponent,
		BasicLayoutComponent,
		BlankLayoutComponent,
		NavigationComponent,
		TopNavbarComponent
	]
})
export class CommonsModule {}
