import { Component, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { AuthenticateService } from '../../core/services/authenticate.service';
import * as baseConstants from '../../core/base-constants';
import { NotifService } from '../../core/services/notif.service';
import { environment } from 'environments/environment';

@Component({
	templateUrl: 'login.component.html'
})
export class LoginComponent {
	@Input() credentials = {username: '', password: ''};

	logoPath = environment.logoPath;

	constructor(private auth: AuthenticateService, private router: Router, private notif: NotifService) {
	}

	login(form: NgForm) {
		if (form.valid) {
			const authenticate = this.auth.authenticate(this.credentials);
			authenticate.subscribe(
				(response: HttpResponse<any>) => {
					this.manageLoginResponse(response);
				},
				(error: HttpErrorResponse) => {
					localStorage.removeItem(baseConstants.cookieToken);
					if (error.status > 0) {
					this.notif.danger(error.error);
					} else {
					this.notif.danger('No se pudo conectar al servidor!');
					}
				}
			);
		}
	}

	manageLoginResponse(response) {
		if (response.status == 200) {
			localStorage.setItem(baseConstants.cookieToken, response.body);
			this.router.navigate(['/starterview']);
		}
	}

	passwordForgotten() {
		// TODO desarrollar modal de resetPassword
	}
}
