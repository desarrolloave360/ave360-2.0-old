import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, PRIMARY_OUTLET } from '@angular/router';

interface IBreadcrumb {
	displayName: string;
	abstract: boolean;
	params: Params;
	url: string;
	last: Boolean;
}

@Component({
	selector: 'app-breadcrumb',
	templateUrl: './breadcrumb.component.html',
	styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {
	public breadcrumbs: IBreadcrumb[] = [];
	displayName: String;

	constructor(private activatedRoute: ActivatedRoute, private router: Router) {}

	ngOnInit() {
		const root: ActivatedRoute = this.activatedRoute.root;
		this.breadcrumbs = this.getBreadcrumbs(root);
	}

	private getBreadcrumbs(route: ActivatedRoute, url: string = '', breadcrumbs: IBreadcrumb[] = []): IBreadcrumb[] {
		const ROUTE_DATA_DISPLAY_NAME = 'displayName';
		const ROUTE_DATA_ABSTRACT = 'abstract';
		const children: ActivatedRoute[] = route.children;

		if (children.length === 0) {
			return breadcrumbs;
		}

		for (const child of children) {
			// verify primary route
			if (child.outlet !== PRIMARY_OUTLET) {
			continue;
			}

			if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_DISPLAY_NAME)) {
			return this.getBreadcrumbs(child, url, breadcrumbs);
			}

			const routeURL: string = child.snapshot.url.map(segment => segment.path).join('/');
			url += `/${routeURL}`;

			this.displayName = child.snapshot.data[ROUTE_DATA_DISPLAY_NAME];
			const breadcrumb: IBreadcrumb = {
			displayName: child.snapshot.data[ROUTE_DATA_DISPLAY_NAME],
			abstract: child.snapshot.data[ROUTE_DATA_ABSTRACT],
			params: child.snapshot.params,
			url: url,
			last: child.children.length === 0
			};

			if (breadcrumb.last) {
			if (breadcrumb.displayName == breadcrumbs[breadcrumbs.length - 1].displayName) {
				breadcrumbs[breadcrumbs.length - 1].last = true;
			} else {
				breadcrumbs.push(breadcrumb);
			}
			} else {
			breadcrumbs.push(breadcrumb);
			}

			return this.getBreadcrumbs(child, url, breadcrumbs);
		}
	}

}
