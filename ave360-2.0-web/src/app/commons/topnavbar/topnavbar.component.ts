import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';


import * as baseConstants from '../../core/base-constants';
import { smoothlyMenu } from '../../app.helpers';
import { RestService } from '../../core/services/rest.service';
import { environment } from 'environments/environment';

declare var jQuery: any;

@Component({
	selector: 'app-top-navbar',
	templateUrl: './topnavbar.component.html',
	styleUrls: ['./topnavbar.component.css']
})
export class TopNavbarComponent implements OnInit {

	currentUser = {
	tieneWildcard: true,
	persona: {nombreCompleto: 'Ave360 Usuario Test'}
	};

	constructor(private httpClient: HttpClient, private router: Router, private rest: RestService) {}

	public ngOnInit(): any {
	this.rest.get('user/getCurrentUser')
		.subscribe(
		(response: HttpResponse<any>) => {
			if (response.type == 4) {

			}
		},
		(errorData: HttpErrorResponse) => {
			localStorage.removeItem(baseConstants.cookieToken);
			this.router.navigate(['/login']);
		}
		);
	}

	toggleNavigation(): void {
		jQuery('body').toggleClass('mini-navbar');
		jQuery('app-navigation-renderer').toggleClass('collapse');
		smoothlyMenu();
	}

	goToSettings() {

	}

	changePassword() {

	}

	logout() {
		const req = new HttpRequest('POST', environment.baseUrl + 'authentication/logout', null, {withCredentials: true});
		this.httpClient.request(req).subscribe(
			(response: HttpResponse<any>) => {
				localStorage.removeItem(baseConstants.cookieToken);
				this.router.navigate(['/login']);
			}
		);
	}

}
