import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-navigation-renderer',
	templateUrl: './navigation-renderer.component.html'
})
export class NavigationRendererComponent implements OnInit {
	@Input('menu') node: {abstracto?, nombre?, nodes?, nivel?, icono?};

	constructor(private router: Router) { }

	ngOnInit() {

	}

	activeRoute(routename: string): boolean {
		return this.router.url.indexOf(routename) > -1;
	}

}
