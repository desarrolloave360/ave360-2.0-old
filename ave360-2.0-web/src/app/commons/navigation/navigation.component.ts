import { Component, AfterViewInit } from '@angular/core';
import {Router} from '@angular/router';
import 'jquery-slimscroll';
import { environment } from 'environments/environment';

declare var jQuery: any;

@Component({
	selector: 'app-navigation',
	templateUrl: 'navigation.component.html'
})

export class NavigationComponent implements AfterViewInit {
	menuReady: Boolean = true;
	logoPath = environment.logoPath;
	menusArray = [
		{
		abstracto: true,
		estado: true,
		fechaCreacion: 1517523672730,
		icono: 'fa-cogs',
		id:  20,
		menuAccion: [],
		nivel: 0,
		nodes: [
			{
			abstracto: true,
			estado: true,
			fechaCreacion: 1517523672730,
			icono: 'fa-money',
			id: 23,
			menuAccion: [],
			nivel: 1,
			nodes: [
				{
				abstracto: false,
				estado: true,
				fechaCreacion: 1517523672730,
				icono: 'fa-user',
				id: 24,
				menuAccion: [],
				nivel: 2,
				nodes: [],
				nombre: 'Comisionistas',
				orden: 'VAKF',
				seleccionado: false,
				// url:'param/commissions/commissionAgents',
				url: 'param.commissions.commissionAgents',
				usuarioCreacion: -1
				}
			],
			nombre: 'Comisiones',
			orden: 'VAK',
			seleccionado: false,
			url: 'param/commissions',
			usuarioCreacion: -1,
			},
		],
		nombre: 'Parametrización',
		orden: 'VA',
		seleccionado: false,
		url: 'param',
		usuarioCreacion: -1}
	];

	constructor(private router: Router) {}

	ngAfterViewInit() {
		jQuery('#side-menu').metisMenu();

		if (jQuery('body').hasClass('fixed-sidebar')) {
			jQuery('.sidebar-collapse').slimscroll({
			height: '100%'
			})
		}
	}

	activeRoute(routename: string): boolean {
		return this.router.url.indexOf(routename) > -1;
	}

}
