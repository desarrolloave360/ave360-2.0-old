import { NgModule } from '@angular/core';

import { CoreModule } from '../../core/core.module';
import { CommissionAgentModule } from './commission-agent/commision-agent.module';

@NgModule({
	imports: [
		CoreModule,
		CommissionAgentModule
	]
})
export class CarClaimsModule {}
