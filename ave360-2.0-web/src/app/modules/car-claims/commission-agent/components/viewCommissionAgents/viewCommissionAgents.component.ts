import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { GetDTOptionsService } from '../../../../../core/services/get-dtoptions.service';
import { RestService } from '../../../../../core/services/rest.service';
import { StatusSelectorComponent } from '../../../../../core/components/status-selector/status-selector.component';



@Component({
	templateUrl: 'viewCommissionAgents.component.html'
})
export class ViewCommissionAgentsComponent implements OnInit, OnDestroy {

	@ViewChild(StatusSelectorComponent) statusSelector: StatusSelectorComponent;
	@ViewChild(DataTableDirective) dtElement: DataTableDirective;

	totalPages: number;
	isLoadingSearch = true;
	commissionAgents = [];
	search: {status?: boolean, currentPage?: number, endDate?, startDate?: Date, text?: string} = {currentPage: 1};
	dtOptions: DataTables.Settings = this.getDTOptions.paged();
	dtTrigger: Subject<any> = new Subject();

	constructor(private getDTOptions: GetDTOptionsService, private rest: RestService, private router: Router, private route: ActivatedRoute) {}

	ngOnInit() {
		this.find();
	}

	ngOnDestroy() {
		this.dtTrigger.unsubscribe();
	}

	ViewCommission(id: number) {

	}

	EditCommission(id: number) {
		this.router.navigate(['editCommission', id], {relativeTo: this.route});
	}

	changeStatus(id: number) {

	}

	addCommissionAgent() {

	}

	find() {
		this.search.status = this.statusSelector.getStatus();
		this.isLoadingSearch = true;
		this.isLoadingSearch = false;
		this.dtOptions.columnDefs.push(this.getDTOptions.notSortable(4));
		this.dtTrigger.next();
		// this.rest.post('commissionAgent/findCommissionAgents',this.search).subscribe(
		// (response: HttpResponse<any>) => {
		// 	if(response.type == 4){
		// 	this.commissionAgents = this.getDTOptions.setPagedData(response.body);
		// 	if(this.dtElement.dtInstance == null) {
		// 		this.dtOptions.columnDefs.push(this.getDTOptions.notSortable(4));
		// 		this.dtTrigger.next();
		// 	}else{
		// 		this.getDTOptions.rerender(this);
		// 	}
		// 	this.totalPages = response.body.totalElements;
	// 		this.isLoadingSearch = false;
		// 	}
		// }
		// );
	}

	clear() {
		this.search = {currentPage: 1};
		this.statusSelector.setStatusToNull();
		this.find();
	}

	pageChanged(page: number) {
		if (page != null) {
		this.search.currentPage = page;
		this.find();
		}
	}

}
