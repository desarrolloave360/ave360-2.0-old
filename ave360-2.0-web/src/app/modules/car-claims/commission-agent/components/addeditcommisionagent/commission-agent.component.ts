import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { NgbModal, NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';

import { RestService } from '../../../../../core/services/rest.service';
import { AddCommissionComponent } from '../addcommission/add-commission.component';
import { SweetService } from '../../../../../core/services/sweet.service';
import { GetDTOptionsService } from '../../../../../core/services/get-dtoptions.service';

@Component({
	templateUrl: './commission-agent.component.html',
	entryComponents: [AddCommissionComponent]
})
export class CommissionAgentComponent implements OnInit, OnDestroy {

	@ViewChild(DataTableDirective) dtElement: DataTableDirective;

	persona: Persona = {};
	commissionAgent: {comisiones?: Array<any>, persona?: Persona, aplicaIva?: boolean} = {persona: this.persona};
	fechaActual = new Date();
	esJur = false;
	view = false;
	dtOptions: DataTables.Settings = this.getDTOptions.unpaged();
	dtTrigger: Subject<any> = new Subject();
	prueba = false;

	constructor(private rest: RestService,
				private getDTOptions: GetDTOptionsService,
				private route: ActivatedRoute,
				private router: Router,
				private sweet: SweetService,
				private modalService: NgbModal) {}

	ngOnInit() {
	this.dtOptions.columnDefs.push(this.getDTOptions.notSortable(7));
	let id;
	this.route.params.subscribe((params: Params) => {
		id = +params['id'];
	});
	this.rest.get('commissionAgent/findOne', {id: id}).subscribe((response: HttpResponse<any>) => {
		if (response.type == 4) {
		this.commissionAgent = response.body;
		if (this.commissionAgent.persona.esJuridico) {
			this.esJur = true;
		}
		this.dtTrigger.next();
		}
	});
	}

	ngOnDestroy() {
	this.dtTrigger.unsubscribe();
	}

	save(form: NgForm) {
	if (form.valid) {
		if (this.commissionAgent.comisiones == null) {
		this.sweet.error('No se ingresaron comisiones para el comisionista.');
		} else {
			this.sweet.save((scope) => {
				scope.rest.post('commissionAgent/save', scope.commissionAgent).subscribe(
				(response: HttpResponse<any>) => {
					if (response.type == 4) {
					scope.router.navigate([scope.route.snapshot.parent.routeConfig.path]);
					scope.sweet.close();
					}
				},
				(errorData: HttpErrorResponse) => {
					console.error(errorData);
				}
				);
			}, this);
		}
	}
	}

	addCommission() {
	this.modalService.open(AddCommissionComponent).result.then(
		(result) => {
		if (this.commissionAgent.comisiones == null) {
			this.commissionAgent.comisiones = [];
		}
		this.commissionAgent.comisiones.push(result);
		this.getDTOptions.rerender(this);
		},
		(reason) => {}
	);
	}

	changeStatus(commission, tooltip: NgbTooltip) {
	tooltip.close();
	this.sweet.changeStatus().then((result) => {
		if (result.value) {
		commission.estado = !commission.estado;
		}
	});
	}

	goBack() {
	this.router.navigate([this.route.snapshot.parent.routeConfig.path]);
	}

}

export interface Persona {
	esJuridico?: boolean;
	documento?: string;
	nombres?: string;
	apellidos?: string;
	razonSocial?: string;
	correo?: string;
	celular?: string;
	codigoSadeg?: string;
	telefono?: string;
	esExtranjero?: boolean;
}
