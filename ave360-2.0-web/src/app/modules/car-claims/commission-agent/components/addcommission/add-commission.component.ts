import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';

import { RestService } from '../../../../../core/services/rest.service';

@Component({
	templateUrl: './add-commission.component.html'
})
export class AddCommissionComponent implements OnInit {

	title = 'Agregar Comisión';
	aseguradoraSelector = false;
	branchOffices = [];
	companies = [];
	branches = [];
	bussinessUnits = [];
	commissionTypes = [];
	search: {sucursal?} = {};
	commission:
		{ramo?, persona?, aseguradora?, comision?, tipoComision?, aseguradorasTodas?, ramosTodos?, movimientosTodos?, movimiento?, estado?}
		= {persona: {}, ramosTodos: true};

	constructor(public activeModal: NgbActiveModal, private rest: RestService) {}

	ngOnInit(): void {
		this.rest.get('branchOffice/findForSelector', {active: false}).subscribe(
			(response: HttpResponse<any>) => {
				this.branchOffices = response.body;
			}
		);
		this.rest.get('insuranceCompany/findAll').subscribe(
			(response: HttpResponse<any>) => {
				this.companies = response.body;
			}
		);
		this.rest.get('catalog/findCatalogChildren', {siglas: 'TDM'}).subscribe(
			(response: HttpResponse<any>) => {
				this.bussinessUnits = response.body;
			}
		);
		this.rest.get('catalog/findCatalogChildren', {siglas: 'TIPCOM'}).subscribe(
			(response: HttpResponse<any>) => {
				this.commissionTypes = response.body;
			}
		);
	}

	save(form: NgForm) {
		if (form.valid) {
			delete this.commission.persona;
			this.commission.estado = true;
			this.activeModal.close(this.commission);
		}
	}

	cancel() {
		this.activeModal.dismiss();
	}

	searchBranches() {
		this.branches = [];
		delete this.commission.ramo;
		if (this.commission.aseguradora != null) {
			this.commission.aseguradora.ramos.forEach((cross) => {
				this.branches.push(cross.ramo);
			});
		}
	}

	addClient() {

	}

	allInsuranceCompanies(status: boolean) {
		this.commission.aseguradorasTodas = status;
		this.commission.ramosTodos = true;
		delete this.commission.aseguradora;
		delete this.commission.ramo;
	}
}
