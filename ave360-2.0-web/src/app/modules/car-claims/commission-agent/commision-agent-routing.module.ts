import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BasicLayoutComponent } from '../../../commons/layouts/basicLayout.component';
import { BlankComponent } from '../../../commons/layouts/blank.component';
import { ViewCommissionAgentsComponent } from './components/viewCommissionAgents/viewCommissionAgents.component';
import { CommissionAgentComponent } from './components/addeditcommisionagent/commission-agent.component';

export const commissionAgentRoutes: Routes = [
	{
		path: '', component: BasicLayoutComponent,
		children: [
			{path: '', component: BlankComponent, children: [
				{path: 'param.commissions.commissionAgents', component: BlankComponent, children: [
					{path: '', component: ViewCommissionAgentsComponent, data: {displayName: 'Comisionista', abstract: false}},
					{path: 'editCommission/:id', component: CommissionAgentComponent, data: {displayName: 'Editar comisiones', abstract: false}}
				] ,  data: {displayName: 'Comisionista', abstract: false}}
			] ,  data: {displayName: 'Comisiones', abstract: true}}
		], data: {displayName: 'Parametrización', abstract: true}
	}
];

@NgModule({
	imports: [
		RouterModule.forChild(commissionAgentRoutes)
	],
	exports: [RouterModule]
})
export class CommissionAgentRoutingModule {}
