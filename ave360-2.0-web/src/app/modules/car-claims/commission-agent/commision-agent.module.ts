import { NgModule } from '@angular/core';

import { CoreModule } from '../../../core/core.module';

import { ViewCommissionAgentsComponent } from './components/viewCommissionAgents/viewCommissionAgents.component';
import { CommissionAgentComponent } from './components/addeditcommisionagent/commission-agent.component';
import { AddCommissionComponent } from './components/addcommission/add-commission.component';
import { CommissionAgentRoutingModule } from './commision-agent-routing.module';



@NgModule({
	declarations: [
		ViewCommissionAgentsComponent,
		CommissionAgentComponent,
		AddCommissionComponent
	],
	imports     : [
		CoreModule,
		CommissionAgentRoutingModule
	],
	exports     : [
		ViewCommissionAgentsComponent,
		CommissionAgentComponent,
		AddCommissionComponent
	],
	entryComponents: [
		AddCommissionComponent
	]
})

export class CommissionAgentModule {}
